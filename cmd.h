#ifndef CMD_H
#define CMD_H

/**
 * Meta command processing result 
 */
enum MetaCommandResult_t{
   META_COMMAND_SUCCESS,
   META_COMMAND_UNRECOGNIZED_COMMAND
};


typedef enum MetaCommandResult_t MetaCommandResult;


MetaCommandResult do_meta_command(InputBuffer* input_buffer, Table* table);

#endif
