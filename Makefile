#!/usr/bin/make -f
CFLAGS = -g

OBJECTS = db.o \
	inputbuffer.o \
	stmts.o \
	cmd.o \
	table.o\
	pager.o

all: db

%.o:	%.c %.h
	gcc -c $(CFLAGS) $<

db:	$(OBJECTS)
	gcc $(CFLAGS) $(OBJECTS) -o db

clean:
	rm *.o db
