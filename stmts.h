#ifndef STMTS_H
#define STMTS_H


/**
 * Type of statement
 */
enum StatementType_t {
   STATEMENT_INSERT, STATEMENT_SELECT
};

typedef enum StatementType_t StatementType;


/**
 * Statement structure
 */
struct Statement_t {
   StatementType type;
   Row row_to_insert;
};


typedef struct Statement_t Statement;


/**
 * Prepare result
 */
enum PrepareResult_t{
   PREPARE_SUCCESS,
   PREPARE_NEGATIVE_ID,
   PREPARE_SYNTAX_ERROR,
   PREPARE_STRING_TOO_LONG,
   PREPARE_UNRECOGNIZED_STATEMENT
};

typedef enum PrepareResult_t PrepareResult;


enum ExecuteResult_t{
   EXECUTE_SUCCESS,
   EXECUTE_TABLE_FULL
};

typedef enum ExecuteResult_t ExecuteResult;

/**
 * Prepare an statement for execution
 * @param input_buffer buffer from which statements are to be read.
 * @param statement Type of statement
 */
PrepareResult prepare_statement(InputBuffer* input_buffer, Statement* statement);


/**
 * Execute a statement
 * @param statement Statement to be executed.
 * @param table Table on which to execute.
 */
ExecuteResult execute_statement(Statement* statement, Table* table);

/**
 * Prepare insert statement
 * @param input_buffer buffer from which statements are to be read.
 * @param statement Type of statement
 */
PrepareResult prepare_insert(InputBuffer* input_buffer, Statement* statement);
#endif
