#ifndef TABLE_H
#define TABLE_H

#define COLUMN_USERNAME_SIZE 32
#define COLUMN_EMAIL_SIZE 255

/**
 * Rows for a temporary table
 */
struct Row_t{
   uint32_t id;
   char username[COLUMN_USERNAME_SIZE + 1];
   char email[COLUMN_EMAIL_SIZE + 1];
}__attribute__ ((__packed__));

typedef struct Row_t Row;

const uint32_t ID_SIZE, USERNAME_SIZE, EMAIL_SIZE;
#define size_of_attribute(Struct, Attribute) sizeof(((Struct*)0)->Attribute)

#define ID_OFFSET 0
#define USERNAME_OFFSET (ID_OFFSET+ID_SIZE)
#define EMAIL_OFFSET (USERNAME_OFFSET+USERNAME_SIZE)
#define ROW_SIZE (ID_SIZE+USERNAME_SIZE+EMAIL_SIZE)


/**
 * Table temporary
 */
struct Table_t {
   Pager* pager;
   uint32_t num_rows;
};

typedef struct Table_t Table;



/**
 * Cursor
 */
struct Cursor_t {
   Table* table;
   uint32_t row_num;
   _Bool end_of_table; //position after the last element
};

typedef struct Cursor_t Cursor;

//////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          //
//                                FUNCTIONS                                                 //
//                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////

Table* db_open(const char* filename);

void db_close(Table* table);

Cursor* table_start(Table* table);

Cursor* table_end(Table* table);

/**
 * Serialize row
 */
void serialize_row(Row* source, void* dest);

/**
 * Deserialize row
 */
void deserialize_row(void* source, Row* dest);

void* cursor_value(Cursor* cursor);

void cursor_advance(Cursor* cursor);

void print_row(Row* row);

#endif
