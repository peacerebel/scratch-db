#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "inputbuffer.h"
#include "pager.h"
#include "table.h"
#include "cmd.h"
#include "stmts.h"

/**
 *print prompt 'db:>'
*/
static void print_prompt(){
   printf("db:> ");
}


int main( int argc, char* argv[]) {

   if(argc < 2){
      printf("Database filename required.\n");
      exit(EXIT_FAILURE);
   }

   char* filename = argv[1];
   Table* table = db_open(filename);
   InputBuffer* input_buffer = new_input_buffer();
   while(1){
      print_prompt();
      read_input(input_buffer);

      if(input_buffer->buffer[0] == '.'){
	 switch(do_meta_command(input_buffer, table)){
	 case (META_COMMAND_SUCCESS):
	    continue;
	 case (META_COMMAND_UNRECOGNIZED_COMMAND):
	    printf("unrecognized command %s.\n", input_buffer->buffer);
	    continue;
	 }
      }

      Statement* statement = malloc(sizeof(Statement));
      switch(prepare_statement(input_buffer, statement)){
      case (PREPARE_SUCCESS):
	    break;  
      case (PREPARE_NEGATIVE_ID):
	 printf("ID must be positive.\n");
	 continue;
      case (PREPARE_STRING_TOO_LONG):
	 printf("String too long.\n");
	 continue;
      case (PREPARE_UNRECOGNIZED_STATEMENT):
	 printf("Unrecognized keyword at start of %s.\n", input_buffer->buffer);
	 continue;
      case (PREPARE_SYNTAX_ERROR):
	 printf("Syntax error. Coould not parse statement.\n");
	 continue;
      }

      switch(execute_statement(statement, table)){
      case (EXECUTE_SUCCESS):
	 printf("DONE.\n");
	 break;
      case (EXECUTE_TABLE_FULL):
	 printf("ERROR: Table Full.\n");
	 break;
      }
      free(statement);
   }
   free(table);
   free(input_buffer);
   return 0;
}
