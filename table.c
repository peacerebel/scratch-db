#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
#include <stdbool.h>

#include "pager.h"
#include "table.h"

const uint32_t ID_SIZE = size_of_attribute(Row, id);
const uint32_t USERNAME_SIZE = size_of_attribute(Row, username);
const uint32_t EMAIL_SIZE = size_of_attribute(Row, email);

/**
 * Initialize new table
 */
Table* db_open(const char* filename){

   Pager* pager = pager_open(filename);
   uint32_t num_rows = pager->file_length / ROW_SIZE;

   Table* table = malloc(sizeof(Table));
   table->pager = pager;
   table->num_rows = num_rows;

   return table;
}

/**
 * Close db on user exit
 */
void db_close(Table* table){
   Pager* pager = table->pager;
   uint32_t num_full_pages = table->num_rows / ROWS_PER_PAGE;

   for(uint32_t i = 0; i < num_full_pages; i++){
      if(pager->pages[i] == NULL){
	 continue;
      }
      pager_flush(pager, i, PAGE_SIZE);
      free(pager->pages[i]);
      pager->pages[i] == NULL;
   }

   uint32_t num_additional_pages = table->num_rows % ROWS_PER_PAGE;
   if(num_additional_pages > 0){
      uint32_t page_num = num_full_pages;
      if(pager->pages[page_num] != NULL){
	 pager_flush(pager, page_num, num_additional_pages * ROW_SIZE);
	 free(pager->pages[page_num]);
	 pager->pages[page_num] == NULL;
      }
   }

   int result = close(pager->file_des);
   if(result == -1){
      printf("Error closing db file.\n");
      exit(EXIT_FAILURE);
   }
   /*for(uint32_t i = 0; i < TABLE_MAX_PAGES; i++){
      void* page = pager->pages[i];
      if(page){
	 free(page);
	 pager->pages[i] == NULL;
       }
    }*/
   free(pager);
}

/**
 * Create new Cursor at the start of the table
 */
Cursor* table_start(Table* table){

   Cursor* cursor = malloc(sizeof(*cursor));
   cursor->table = table;
   cursor->row_num = 0;
   cursor->end_of_table = (table->num_rows == 0);

   return cursor;
}


/**
 * Create new Cursor at the start of the table
 */
Cursor* table_end(Table* table){

   Cursor* cursor = malloc(sizeof(*cursor));
   cursor->table = table;
   cursor->row_num = table->num_rows;
   cursor->end_of_table = true;

   return cursor;
}


/**
 * Serialize row
 */
void serialize_row(Row* source, void* dest){
   memcpy(dest + ID_OFFSET, &(source->id), ID_SIZE);
   memcpy(dest + USERNAME_OFFSET, &(source->username), USERNAME_SIZE);
   memcpy(dest + EMAIL_OFFSET, &(source->email), EMAIL_SIZE);
}

/**
 * Deserialize row
 */
void deserialize_row(void* source, Row* dest){
   //printf("id = %s\n uname = %s\n email = %s\n",  dest->id, dest->username, dest->email);
   memcpy(&(dest->id), source + ID_OFFSET, ID_SIZE);
   memcpy(&(dest->username), source + USERNAME_OFFSET, USERNAME_SIZE);
   memcpy(&(dest->email), source + EMAIL_OFFSET, EMAIL_SIZE);
}
/**
 * Get the cursor value
 */

void* cursor_value(Cursor* cursor){
   uint32_t row_num = cursor->row_num;
   uint32_t page_num = row_num/ROWS_PER_PAGE;
   
   void* page = get_page(cursor->table->pager, page_num);

   uint32_t row_offset = row_num % ROWS_PER_PAGE;
   uint32_t byte_offset = row_offset * ROW_SIZE;
   //printf("page = %d\n row_num = %d\n row_offset = %d \n byte_offset = %d\n ROW_SIZE = %d\n ROWS_PER_PAGE = %d\n", page, row_num, row_offset.rem, byte_offset, ROW_SIZE,  ROWS_PER_PAGE);
   return page + byte_offset;
}


/**
 * Advancing cursor
 */
void cursor_advance(Cursor* cursor){

   cursor->row_num +=1;
   if(cursor->row_num >= cursor->table->num_rows){
      cursor->end_of_table = true;
   }
}


/**
 * Print a row
 */
void print_row(Row* row) {
   printf("(%d, %s, %s)\n", row->id, row->username, row->email);
}
