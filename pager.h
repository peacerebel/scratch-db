#ifndef PAGER_H
#define PAGER_H

#define PAGE_SIZE 4096
#define TABLE_MAX_PAGES 100
#define ROWS_PER_PAGE (PAGE_SIZE/ROW_SIZE)
#define TABLE_MAX_ROWS (ROWS_PER_PAGE*TABLE_MAX_PAGES)


struct Pager_t {
   int file_des;
   uint32_t file_length;
   void* pages[TABLE_MAX_PAGES];
};

typedef struct Pager_t Pager;

Pager* pager_open(const char* filename);

void* get_page(Pager* pager, uint32_t page_num);

void pager_flush(Pager* pager, uint32_t page_num, uint32_t size);
#endif
