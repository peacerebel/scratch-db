#ifndef INPUTBUFFER_H
#define INPUTBUFFER_H


/**
 * stores the input statement
 */
struct InputBuffer_t{
   char* buffer;
   size_t buffer_length;
   ssize_t input_length;
};

typedef struct InputBuffer_t InputBuffer;


/**
 * Initialize a new input buffer
 */
InputBuffer* new_input_buffer();


/**
 * Read statements to input_buffer
 * @param input_buffer Pointer to the buffer to write statement from the input stream
 */
void read_input(InputBuffer* input_buffer);

#endif
